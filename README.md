Hey, Alter! Setupserver
=======================

Ein auf Docker basierender Server, welcher die Erstellung eines HeyAlter Setup-WLANs/Netzwerk ermöglicht.

Funktionalitäten, die der Server bereitstellt:
- apt-cacher-ng zum lokalen Cachen von Updates
- dnsmasq für DHCP und DNS im HeyAlter Setup-Netzwerk
- nginx zum Ausliefern von Dateien (hinter der lokalen set.up-Domain)
- SFTP-Server zum Hochladen von Computerspecs


> Das Repository ist derzeitig nur ein Dump des Setups von BS und daher noch sehr spezifisch! Benutzung wird noch nicht empfohlen.

Nutzung
-------

- Rechner aufsetzen mit zwei Netzwerken: Uplink und das HeyAlter-Setupnetzwerk mit der statischen IP 192.168.123.1/24
- Beispielsetup
	- Docker und Docker-Compose auf einem HA-Rechner installieren
	- HA-Rechner mit einem USB-Ethernet-Adapter ausstatten
	- An einen Netzwerkport das Internet (aka Uplink) anschließen, den anderen an einen WLAN-Router ohne DHCP anschließen, der das HeyAlter Setup-Netzwerk bereitstellt
- enp7s0 in `dnsmasq/config` auf entsprechenden Ethernet-Adapter des Setupnetzwerkes ändern
- 192.168.13.31 in `docker-compose.yml` durch Uplink-IP austauschen oder ganze Zeile löschen
- Server mit `sudo docker-compose up -d` starten (`sudo docker-compose down` zum stoppen)

TODO
----

- Configstuff in eine `.env`-Datei packen
- ntpd-Dienst nachtragen
- dnsmasq `no-resolv` fixen
- Spec-Server auf was write-only umprogrammieren
