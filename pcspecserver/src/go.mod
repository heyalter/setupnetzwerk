module pcspecserver

go 1.17

require (
	github.com/pkg/sftp v1.13.4
	golang.org/x/crypto v0.0.0-20220131195533-30dcbda58838
)

require (
	github.com/kr/fs v0.1.0 // indirect
	golang.org/x/sys v0.0.0-20210615035016-665e8c7367d1 // indirect
)
