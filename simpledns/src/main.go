package main

import (
	"fmt"
	"log"

	"github.com/miekg/dns"
)

var records = map[string]string{
	"set.up.": "192.168.123.1",
	"de.archive.ubuntu.com.": "192.168.123.1",
	"archive.ubuntu.com.": "192.168.123.1",
	"security.ubuntu.com.": "192.168.123.1",
	"ntp.ubuntu.com.": "192.168.123.1",
}

func handleQuery(m *dns.Msg) {
	for _, q := range m.Question {
		switch q.Qtype {
		case dns.TypeA:
			log.Printf("Query for %s\n", q.Name)
			ip := records[q.Name]
			if ip != "" {
				rr, err := dns.NewRR(fmt.Sprintf("%s A %s", q.Name, ip))
				if err == nil {
					m.Answer = append(m.Answer, rr)
				}
			}
		}
	}
}

func handleDnsRequest(w dns.ResponseWriter, r *dns.Msg) {
	m := &dns.Msg{}
	m.SetReply(r)

	if r.Opcode == dns.OpcodeQuery {
		handleQuery(m)
	}

	w.WriteMsg(m)
}

func main() {
	server := &dns.Server{
		Addr: ":5353",
		Net: "udp",
		Handler: dns.HandlerFunc(handleDnsRequest),
	}
	defer server.Shutdown()
	log.Fatal(server.ListenAndServe())
}
